package ru.leorikwhite.decomposestudyapp.compose

import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import ru.leorikwhite.decomposestudyapp.components.ToolbarComponent

@Composable
fun ToolbarUi(component: ToolbarComponent) {

    val progress by component.passingPercent.collectAsState()

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
            .clickable { component::onHintClick },

    ) {
        LinearProgressIndicator(
            progress = progress.toFloat(),
            modifier = Modifier.fillMaxWidth()
                .height(30.dp)
                .clip(RoundedCornerShape(5.dp))
        )
        Text(
            text = "${(progress)}%",
            modifier = Modifier.align(Alignment.Center)
        )
    }
}
