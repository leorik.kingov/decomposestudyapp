package ru.leorikwhite.decomposestudyapp.compose

import android.graphics.fonts.FontStyle
import android.text.Layout.Alignment
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import kotlinx.coroutines.Dispatchers
import ru.leorikwhite.decomposestudyapp.components.ListTestsComponent
import ru.leorikwhite.decomposestudyapp.data.Test

@Composable
fun ListTestsUi(component: ListTestsComponent){

    val tests by component.tests.collectAsState(Dispatchers.Main.immediate)

    val easyTests = tests.filter { it.difficulty == "easy" }
    val mediumTests = tests.filter { it.difficulty == "medium" }
    val hardTests = tests.filter { it.difficulty == "hard" }

    Column(
        Modifier.padding(10.dp)
    ){
        //EasyTests
        Heading(head = "Easy", twoHead = "step 1")
        LazyRow(){
            items(easyTests){
                test -> TestItem(test = test)
            }
        }
        //MediumTests
        Heading(head = "Medium", twoHead = "step 2")
        LazyRow(){
            items(mediumTests){
                test -> TestItem(test = test)
            }
        }
        //HardTests
        Heading(head = "Hard", twoHead = "step 3")
        LazyRow(){
            items(hardTests){
                test -> TestItem(test = test)
            }
        }
    }

}

@Composable
fun TestItem(test: Test){
    Card(
        Modifier
            .padding(5.dp)
    ){
        Image(
            painterResource(id = test.imageId),
            contentDescription = null,
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .clip(
                    RoundedCornerShape(10.dp)
                )
                .width(200.dp)
                .height(200.dp)
        )
        Text(
            text = test.name,
            fontSize = 18.sp,
            fontWeight = FontWeight.Bold,
            modifier = Modifier
                .padding(top = 10.dp, start = 10.dp)
        )
        Text(
            text = "Questions " + test.countQuestions,
            fontSize = 16.sp,
            modifier = Modifier
                .padding(top = 5.dp,start = 10.dp, bottom = 5.dp)
        )
    }
}

@Composable
fun Heading(head:String,twoHead:String){
    Row(){
        Text(
            text = head,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.padding(end = 5.dp)
        )
        Box(modifier = Modifier
            .clip(RoundedCornerShape(100))
            .background(Color.LightGray)){
            Text(
                text = twoHead.uppercase(),
                fontWeight = FontWeight.Bold,
                modifier = Modifier
                    .padding(start = 5.dp, end = 5.dp)
            )
        }

        Row(
            horizontalArrangement = Arrangement.End,
            modifier = Modifier.fillMaxWidth()
        ){
            Text(
                text = "See All",
                color = Color.Cyan
            )
        }
    }
}