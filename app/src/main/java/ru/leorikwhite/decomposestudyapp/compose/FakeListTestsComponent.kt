package ru.leorikwhite.decomposestudyapp.compose

import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import kotlinx.coroutines.flow.MutableStateFlow
import ru.leorikwhite.decomposestudyapp.R
import ru.leorikwhite.decomposestudyapp.components.ListTestsComponent
import ru.leorikwhite.decomposestudyapp.data.Test

class FakeListTestsComponent : ListTestsComponent {

    override val tests = MutableStateFlow(listOf(
        Test(0,"Test1","easy", R.drawable.test1,"0-10"),
        Test(1,"Test2","easy", R.drawable.test2,"10-20"),
        Test(2,"Test3","easy", R.drawable.test3,"20-30"),
        Test(3,"Test4","medium", R.drawable.test4,"30-40"),
        Test(4,"Test5","medium", R.drawable.test5,"40-50"),
        Test(5,"Test6","medium", R.drawable.test6,"50-60"),
        Test(6,"Test7","hard", R.drawable.test7,"60-70"),
        Test(7,"Test8","hard", R.drawable.test8,"70-80"),
        Test(8,"Test9","hard", R.drawable.test9,"80-90"),
    ))

    override fun onTestClick(id: Int) {
        TODO("Not yet implemented")
    }

}

@Preview(showSystemUi = true)
@Composable
fun ListTestsUiPreview() {
    ListTestsUi(FakeListTestsComponent())
}
