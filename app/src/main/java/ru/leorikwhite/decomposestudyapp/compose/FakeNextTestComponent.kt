package ru.leorikwhite.decomposestudyapp.compose

import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import kotlinx.coroutines.flow.MutableStateFlow
import ru.leorikwhite.decomposestudyapp.components.NextTestComponent

class FakeNextTestComponent : NextTestComponent{

    override val name = MutableStateFlow("Next Super Test")

    override fun onNextTestClick() {
        TODO("Not yet implemented")
    }

}

@Preview(showSystemUi = true)
@Composable
fun NextTestUiPreview() {
    NextTestUi(FakeNextTestComponent())
}
