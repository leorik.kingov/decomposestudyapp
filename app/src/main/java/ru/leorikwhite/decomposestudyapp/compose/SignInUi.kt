package ru.leorikwhite.decomposestudyapp.compose

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import kotlinx.coroutines.Dispatchers
import ru.leorikwhite.decomposestudyapp.components.SignInComponent

@Composable
fun SignInUi(component: SignInComponent) {

    val login by component.login.collectAsState(Dispatchers.Main.immediate)
    val password by component.password.collectAsState(Dispatchers.Main.immediate)
    val inProgress by component.inProgress.collectAsState()

    Column {
        TextField(
            value = login,
            onValueChange = component::onLoginChanged
        )

        TextField(
            value = password,
            onValueChange = component::onPasswordChanged,
            visualTransformation = PasswordVisualTransformation(),
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password)
        )

        if (inProgress) {
            CircularProgressIndicator()
        } else {
            Button(onClick = component::onSignInClick){}
        }
    }
}