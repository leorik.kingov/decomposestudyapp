package ru.leorikwhite.decomposestudyapp.compose

import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import ru.leorikwhite.decomposestudyapp.components.ToolbarComponent

class FakeToolbarComponent : ToolbarComponent {

    override val passingPercent: StateFlow<Int> = MutableStateFlow(10)

    override fun onHintClick() {
        TODO("Некая логика")
    }
}

@Preview(showSystemUi = true)
@Composable
fun ToolbarUiPreview() {
    ToolbarUi(FakeToolbarComponent())
}