package ru.leorikwhite.decomposestudyapp.compose

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.blur
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.imageResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import kotlinx.coroutines.Dispatchers
import ru.leorikwhite.decomposestudyapp.R
import ru.leorikwhite.decomposestudyapp.components.NextTestComponent


@Composable
fun NextTestUi(component: NextTestComponent) {
    val nextTestName by component.name.collectAsState(Dispatchers.Main.immediate)
    val padding = 10.dp
    Box(
        modifier = Modifier
            .padding(10.dp)
    ) {
        Card(
            Modifier
                .fillMaxWidth()
                .height(350.dp)
                .clip(RoundedCornerShape(15.dp))
        ) {
        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            Image(
                painterResource(id = R.drawable.next_test_background), contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier.fillMaxSize()
                    .blur(3.dp)
            )
            Column {
                Text(
                    text = "take me next".uppercase(),
                    Modifier
                        .padding(start = padding, top = padding),
                    color = Color.White
                )
                Text(
                    text = nextTestName,
                    Modifier
                        .padding(start = padding, top = padding),
                    fontWeight = FontWeight.Bold,
                    fontSize = 20.sp,
                    color = Color.White
                )
                Column (
                    verticalArrangement = Arrangement.Bottom,
                    modifier = Modifier.fillMaxSize()
                ){
                    Text(
                        text = "Use this card to proceed. It'll always point to the next test you need to take.",
                        Modifier
                            .padding(start = padding, bottom = padding * 2, end = padding),
                        fontSize = 14.sp,
                        color = Color.White
                    )
                }
            }
        }
        }

    }
}