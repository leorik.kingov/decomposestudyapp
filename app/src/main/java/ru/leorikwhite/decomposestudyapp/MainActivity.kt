package ru.leorikwhite.decomposestudyapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.arkivanov.decompose.defaultComponentContext
import ru.leorikwhite.decomposestudyapp.components.Main.MainComponent
import ru.leorikwhite.decomposestudyapp.components.RealSignInComponent
import ru.leorikwhite.decomposestudyapp.compose.ListTestsUi
import ru.leorikwhite.decomposestudyapp.compose.NextTestUi
import ru.leorikwhite.decomposestudyapp.compose.SignInUi
import ru.leorikwhite.decomposestudyapp.compose.ToolbarUi
import ru.leorikwhite.decomposestudyapp.data.AuthorizationRepository
import ru.leorikwhite.decomposestudyapp.ui.theme.DecomposeStudyAppTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        
        val rootComponent = RealSignInComponent(defaultComponentContext(),
            AuthorizationRepository()
        )
        
        setContent {
            SignInUi(component = rootComponent)


        }
    }
}

@Composable
fun MainUi(component: MainComponent) {
    Scaffold(
        topBar = { ToolbarUi(component = component.toolbarComponent) },
        content = {
            innerPadding ->
            Column(modifier = Modifier
                .verticalScroll(rememberScrollState())
                .padding(innerPadding)
            ){

                NextTestUi(component = component.nextTestComponent)

                ListTestsUi(component = component.testsComponent)

            }
        }
    )
}