package ru.leorikwhite.decomposestudyapp.data

data class Test(
    val id:Int,
    val name:String,
    val difficulty:String,
    val imageId:Int,
    val countQuestions:String
)
