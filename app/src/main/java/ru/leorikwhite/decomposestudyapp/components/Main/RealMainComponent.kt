package ru.leorikwhite.decomposestudyapp.components.Main

import com.arkivanov.decompose.ComponentContext
import com.arkivanov.decompose.childContext
import ru.leorikwhite.decomposestudyapp.components.NextTestComponent
import ru.leorikwhite.decomposestudyapp.components.RealListTestsComponent
import ru.leorikwhite.decomposestudyapp.components.RealNextTestComponent
import ru.leorikwhite.decomposestudyapp.components.RealToolbarComponent
import ru.leorikwhite.decomposestudyapp.components.ToolbarComponent

class RealMainComponent (componentContext : ComponentContext):
ComponentContext by componentContext, MainComponent {

    override val toolbarComponent: ToolbarComponent = RealToolbarComponent(
        childContext(key="toolbar")
    )

    override val nextTestComponent: NextTestComponent = RealNextTestComponent(
        childContext(key="nextTest")
    )

    override val testsComponent = RealListTestsComponent(
        childContext(key = "tests"),

        onTestComplete = {
            nextTestComponent.nextTestUnlock()
        }
    )

}