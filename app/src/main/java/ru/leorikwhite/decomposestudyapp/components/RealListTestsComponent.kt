package ru.leorikwhite.decomposestudyapp.components

import androidx.compose.runtime.mutableStateListOf
import com.arkivanov.decompose.ComponentContext
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import ru.leorikwhite.decomposestudyapp.data.Test

class RealListTestsComponent(componentContext: ComponentContext, onTestComplete:() -> Unit) :
    ComponentContext by componentContext, ListTestsComponent {

    override val tests: StateFlow<List<Test>> = MutableStateFlow(listOf())

//    Test(0,"test","hard",1,"0-0")

    override fun onTestClick(id: Int) {
        TODO("Not yet implemented")
    }

}