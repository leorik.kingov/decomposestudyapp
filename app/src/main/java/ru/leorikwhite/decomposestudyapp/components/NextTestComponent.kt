package ru.leorikwhite.decomposestudyapp.components

import kotlinx.coroutines.flow.StateFlow

interface NextTestComponent {

    val name: StateFlow<String>

    fun onNextTestClick()

    fun nextTestUnlock()

}