package ru.leorikwhite.decomposestudyapp.components

import kotlinx.coroutines.flow.StateFlow
import ru.leorikwhite.decomposestudyapp.data.Test

interface ListTestsComponent {

    val tests: StateFlow<List<Test>>

    fun onTestClick(id:Int)

}