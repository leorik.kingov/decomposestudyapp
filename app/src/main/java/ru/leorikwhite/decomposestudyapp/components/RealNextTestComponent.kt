package ru.leorikwhite.decomposestudyapp.components

import com.arkivanov.decompose.ComponentContext
import kotlinx.coroutines.flow.MutableStateFlow

class RealNextTestComponent(componentContext: ComponentContext) :
    ComponentContext by componentContext, NextTestComponent {

    override val name = MutableStateFlow("Next test name")

    override fun onNextTestClick() {
        TODO("Not yet implemented")
    }

    override fun nextTestUnlock() {
        TODO("Not yet implemented")
    }
}