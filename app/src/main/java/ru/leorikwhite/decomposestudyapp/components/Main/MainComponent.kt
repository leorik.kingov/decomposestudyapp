package ru.leorikwhite.decomposestudyapp.components.Main

import ru.leorikwhite.decomposestudyapp.components.ListTestsComponent
import ru.leorikwhite.decomposestudyapp.components.NextTestComponent
import ru.leorikwhite.decomposestudyapp.components.ToolbarComponent

interface MainComponent {

    val toolbarComponent:ToolbarComponent

    val nextTestComponent:NextTestComponent

    val testsComponent:ListTestsComponent

}