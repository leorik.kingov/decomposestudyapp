package ru.leorikwhite.decomposestudyapp.components

import kotlinx.coroutines.flow.StateFlow

interface ToolbarComponent {

    val passingPercent: StateFlow<Int>

    fun onHintClick()

}