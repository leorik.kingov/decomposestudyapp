package ru.leorikwhite.decomposestudyapp.components

import com.arkivanov.decompose.ComponentContext
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class RealToolbarComponent(componentContext: ComponentContext) :
    ComponentContext by componentContext, ToolbarComponent {

    override val passingPercent: StateFlow<Int> = MutableStateFlow(0)

    override fun onHintClick() {
        TODO("Некая логика")
    }
}