package ru.leorikwhite.decomposestudyapp.components

import com.arkivanov.decompose.ComponentContext
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import ru.leorikwhite.decomposestudyapp.data.AuthorizationRepository

class RealSignInComponent(
    componentContext: ComponentContext,
    private val authorizationRepository: AuthorizationRepository
) : ComponentContext by componentContext, SignInComponent {

    override val login = MutableStateFlow("")

    override val password = MutableStateFlow("")

    override val inProgress = MutableStateFlow(false)


    override fun onLoginChanged(login: String) {
        this.login.value = login
    }

    override fun onPasswordChanged(password: String) {
        this.password.value = password
    }

    override fun onSignInClick() {
        CoroutineScope(Dispatchers.IO).launch {
            inProgress.value = true
            authorizationRepository.signIn(login.value, password.value)
            inProgress.value = false

            // TODO: navigate to the next screen
        }


    }
}